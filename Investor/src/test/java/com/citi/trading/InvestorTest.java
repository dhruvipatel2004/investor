package com.citi.trading;

import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import com.citi.trading.Investor;
import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;

public class InvestorTest{
	
	public static Investor invest;
	
	public static class MockMarket implements OrderPlacer {
		public void placeOrder (Trade order, Consumer<Trade> callback) {
			callback.accept(order);
		}
	}
	
	@Before
	public void setup() {
		OrderPlacer mock = new MockMarket();
		invest = new Investor(20000, mock);
		invest.buy("MRK", 100, 60);
		invest.buy("APP", 200, 50);
	}
	
	@Test
	public void testPortfolio() throws Exception {		
		assertThat(invest.getCash(), closeTo(4000, 0.00001));
		assertThat(invest.getPortfolio(), both(hasEntry("MRK", 100)).and(hasEntry("APP", 200)));
	}
	
	@Test
	public void testCash() throws Exception  {
		assertThat(invest.getCash(), closeTo(4000, 0.00001));
		invest.buy("GOOG", 100, 20);
		assertThat(invest.getCash(), closeTo(2000, 0.00001));
		assertThat(invest.getPortfolio(), both(hasEntry("MRK", 100)).and(hasEntry("APP", 200)).and(hasEntry("GOOG",100)));
	}
}

